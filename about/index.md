---
layout:     page
title:      About
class:      'post'
navigation: True
logo:       'assets/images/PayRnD.png'
current:    about
---

PayRnD.org will be created as a non-profit
[Distributed / Decentralised Autonomous Organisation](https://en.wikipedia.org/wiki/Decentralized_autonomous_organization)
on the
[Aragon Network](https://aragon.org/).

It's primary purpose is to fund brain research, by, but not limited to, the
[Neural Archives Foundation](http://neuralarchivesfoundation.org), an Australian non-profit:

"The NAF is a registered charity with the Australian Charities and Not-for-profits Commission (ACNC) and is endorsed as a Deductible Gift Recipient (DGR) with the Australian Tax Office (ATO) from 23 February 2018."

Research will cover the following areas:

- Improving neural tissue preservation methods
- High-resolution brain scanning
- Extraction of meaningful information from living and frozen neural tissue
- Brain Communication Interfaces (BCIs)
- Uploading human and other mammalian consciousnesses into non-biological substrates

If you would like to be involved in the development of this project, contact Philip Rhoades:  phr AT payrnd DOT org



